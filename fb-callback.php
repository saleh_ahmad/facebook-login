<?php
session_start();
require_once 'vendor/autoload.php';

$fb = new Facebook\Facebook([
    'app_id'                => '1745444622384181', // Replace {app-id} with your app id
    'app_secret'            => 'fb3f043f18f5cd317832dcf1fab8e01e',
    'default_graph_version' => 'v2.2',
]);

$helper = $fb->getRedirectLoginHelper();

try {
    $accessToken = $helper->getAccessToken();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    /** When Graph returns an error */
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    /** When validation fails or other local issues */
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

try {
    /** Returns a `Facebook\FacebookResponse` object */
    $response = $fb->get('/me', $accessToken);
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

$user = $response->getGraphUser();

$_SESSION['name']             = $user->getName();
$_SESSION['id']               = $user->getId();
$_SESSION['firstname']        = $user->getFirstName();
$_SESSION['middlename']       = $user->getMiddleName();
$_SESSION['lastname']         = $user->getLastName();
$_SESSION['link']             = $user->getLink();
$_SESSION['dob']              = $user->getBirthday();
$_SESSION['location']         = $user->getLocation();
$_SESSION['hometown']         = $user->getHometown();
$_SESSION['significantother'] = $user->getSignificantOther();

if (! isset($accessToken)) {
    if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
    }
    exit;
}

// Logged in
/*echo '<h3>Access Token</h3>';
echo '<pre>';
var_dump($accessToken->getValue());
echo '</pre>';*/

/** The OAuth 2.0 client handler helps us manage access tokens */
$oAuth2Client = $fb->getOAuth2Client();

/** Get the access token metadata from /debug_token */
$tokenMetadata = $oAuth2Client->debugToken($accessToken);
/*echo '<h3>Metadata</h3>';
echo '<pre>';
var_dump($tokenMetadata);
echo '</pre>';*/

/** Validation (these will throw FacebookSDKException's when they fail) */
$tokenMetadata->validateAppId('1745444622384181');
/**
 * If you know the user ID this access token belongs to, you can validate it here
 * $tokenMetadata->validateUserId('123');
 */
$tokenMetadata->validateExpiration();

if (!$accessToken->isLongLived()) {
    /** Exchanges a short-lived access token for a long-lived one */
    try {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
        exit;
    }

    /*echo '<h3>Long-lived</h3>';
    echo '<pre>';
    var_dump($accessToken->getValue());
    echo '</pre>';*/
}

$_SESSION['fb_access_token'] = (string) $accessToken;

/**
 * User is logged in with a long-lived access token.
 * You can redirect them to a members-only page.
 */
header('Location: demotest.php');
