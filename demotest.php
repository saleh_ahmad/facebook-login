<?php session_start(); ?>
<!doctype html>
<html lang="en-US">
<head>
    <title>Welcome</title>
</head>
<body>
<header></header>
<section>
    <h1>Welcome!</h1>
    <?php if(isset($_SESSION['fb_access_token'])): ?>
        <table border="1">
            <tr>
                <td>Image</td>
                <td><img src='https://graph.facebook.com/<?= $_SESSION['id'] ?>/picture'></td>
            </tr>
            <tr>
                <td>ID</td>
                <td><?= $_SESSION['id'] ?></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><?= $_SESSION['name'] ?></td>
            </tr>
            <tr>
                <td>First Name</td>
                <td><?= $_SESSION['firstname'] ?></td>
            </tr>
            <tr>
                <td>Middle Name</td>
                <td><?= $_SESSION['middlename'] ?></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><?= $_SESSION['lastname'] ?></td>
            </tr>
            <tr>
                <td>Link</td>
                <td><?= $_SESSION['link'] ?></td>
            </tr>
            <tr>
                <td>Birthday</td>
                <td><?= $_SESSION['dob'] ?></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><?= $_SESSION['location'] ?></td>
            </tr>
            <tr>
                <td>Hometown</td>
                <td><?= $_SESSION['hometown'] ?></td>
            </tr>
            <tr>
                <td>Significant Another</td>
                <td><?= $_SESSION['significantother'] ?></td>
            </tr>
        </table>
    <?php endif; ?>
</section>
<footer>
    <a href="logout.php">Logout</a>
</footer>
</body>
</html>