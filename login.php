<?php
session_start();
require_once 'vendor/autoload.php';

//APP ID: 1745444622384181
//APP Secret: fb3f043f18f5cd317832dcf1fab8e01e

$fb = new Facebook\Facebook([
    'app_id' => '1745444622384181', // Replace {app-id} with your app id
    'app_secret' => 'fb3f043f18f5cd317832dcf1fab8e01e',
    'default_graph_version' => 'v2.2',
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('http://localhost/1353/fb-callback.php', $permissions);
?>

<h3>Press the following button to login using Facebook!</h3>
<a href="<?= htmlspecialchars($loginUrl); ?>">
    <img src="login-with-facebook-button.png" alt="Facebook Login Button" />
</a>';